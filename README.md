Experience big city living in the Little City - Northgate at Falls Church in Falls Church, Virginia offers amazing access to the Washington, D.C. area and some of the nation’s best schools. Walk to the metro to get to work, school, or a night out with friends. Or, stay in to enjoy all the comforts.

Address: 450 N Washington St, Falls Church, VA 22046, USA

Phone: 703-237-0600
